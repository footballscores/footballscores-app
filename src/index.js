import React from 'react';
import ReactDOM from 'react-dom';
import './styles/fonts.css';
import './styles/colors.css';
import './styles/main.css';
import App from './App';
import Axios from 'axios';
require("dotenv").config();

Axios.interceptors.request.use((request) => {
    console.log("--- AXIOS REQUEST START ---");
    console.log(request);
    console.log("--- AXIOS END REQUEST ---");
    return request;
})

ReactDOM.render(
    <App />,
  document.getElementById('root')
);
