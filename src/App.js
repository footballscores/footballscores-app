import './App.css';
import { AnimatedApp } from './AnimatedApp';
import _ from 'lodash';

function App() {

    return (
        <AnimatedApp />
    );
}


export default App;
