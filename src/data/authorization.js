import { config, storage } from "../config/constants";
import { getAccessTokenData } from "../service/oauthService";
import { isExpired } from "./token";
import _ from 'lodash';

export const authorizeUser = async (redirectOffline = true, redirectNoSave = true) => {
    let accessToken = localStorage.getItem(storage.ACCESS_TOKEN);

    if (accessToken) {
        authorizeAccessToken(accessToken, redirectOffline, redirectNoSave);
    } else {
        if (redirectOffline) {
            window.location = config.BASE_URL;
        }
    }
}

const authorizeAccessToken = (token, redirectOffline, redirectNoSave) => {
    let accessTokenExpireDate = localStorage.getItem(storage.ACCESS_TOKEN_EXPIRE_DATE);
    if (accessTokenExpireDate) {
        accessTokenChecks(token, accessTokenExpireDate, redirectOffline, redirectNoSave);
    } else {
        removeTokensFromStorage();
        if (redirectOffline) {
            window.location = config.BASE_URL;
        }
    }
}

const accessTokenChecks = async (token, accessTokenExpireDate, redirectOffline, redirectNoSave) => {
    if (!isExpired(accessTokenExpireDate)) {
        await verifyAccessTokenViaApi(token, redirectOffline, redirectNoSave);
    } else {
        removeTokensFromStorage();
        if (redirectOffline) {
            window.location = config.BASE_URL;
        }
    }
}

const verifyAccessTokenViaApi = async (token, redirectOffline, redirectNoSave) => {
    try {
        let response = await getAccessTokenData(token);
        if (_.isEmpty(response.data)) {
            removeTokensFromStorage();
            if (redirectOffline) {
                window.location = config.BASE_URL;
            }
            return;
        }

        let data = response.data[0];

        if (isExpired(data.access_token_expire_date)) {
            removeTokensFromStorage();
            if (redirectOffline) {
                window.location = config.BASE_URL;
            }
            return;
        }

        let saveId = localStorage.getItem(storage.SAVE_ID);
        if (!saveId) {
            if (redirectNoSave) {
                window.location = config.BASE_URL + "/save";
            }
            return;
        } 

        //If we are on the offline page, but we are actually signed in
        //Redirect to the /overview
        if (!redirectOffline) {
            window.location = config.BASE_URL + "/overview";
            return;
        }

        return;
    } catch (err) {
        window.location = config.BASE_URL;
    }
}

const removeTokensFromStorage = () => {
    localStorage.removeItem(storage.ACCESS_TOKEN);
    localStorage.removeItem(storage.ACCESS_TOKEN_EXPIRE_DATE);
}