import { storage } from "../config/constants";

export const isExpired = expireDateTimestamp => {
    return expireDateTimestamp < new Date().getTime() / 1000;
}

export const resetCodeChallenge = () => {
    localStorage.removeItem(storage.CODE_CHALLENGE);
    localStorage.removeItem(storage.VERIFICATION_CODE);
}