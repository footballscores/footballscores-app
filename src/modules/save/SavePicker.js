import { useEffect, useRef, useState } from "react";
import { storage } from "../../config/constants";
import { authorizeUser } from "../../data/authorization";
import { createSave, getSaves } from "../../service/saveService";
import Header from "../generics/Header";
import _ from 'lodash';
import { Button, Fab } from "../generics/Buttons";
import { TabSheet, TabTrigger } from "../generics/Tabs";
import { FormContainer, Input } from "../generics/Forms";
import { getFriendlyErrorMessage } from "../../service/errorService";
import { SaveCard } from "../generics/Cards";


const SavePicker = ({ props }) => {

    const [saves, setSaves] = useState([]);
    const [saveName, setSaveName] = useState("");
    const [formIsUploading, setFormIsUploading] = useState(false);
    const [formError, setFormError] = useState(null);

    const tabSheetRef = useRef(null);

    useEffect(() => {
        authorizeUser(true, false);
        getSaves(localStorage.getItem(storage.ACCESS_TOKEN))
            .then((response) => {
                if (_.isEmpty(response.data)) {
                    return;
                }
                setSaves(response.data);
            });
    }, [])

    const dismissSheet = () => {
        tabSheetRef.current.classList.remove("visible");
        tabSheetRef.current.classList.add("invisible");
    }

    const handleChange = (value) => {
        setSaveName(value);
    }

    const submitForm = () => {
        setFormIsUploading(true);
        console.log(saveName);
        createSave(localStorage.getItem(storage.ACCESS_TOKEN), saveName)
        .then((response) => {
            setFormIsUploading(false);
            if (!response.data.type) {
                setSaves([...saves, response.data[0]]);
                dismissSheet();
            } else {
                setFormError(getFriendlyErrorMessage(response.data));
            }
        })
        .catch((err) => {
            setFormIsUploading(false);
            setFormError(getFriendlyErrorMessage(err.response.data));
        });
    }

    return (
        <>
            <Header title={"Pick a save"} />

            {saves.map((value, index) => {
                return (
                    <SaveCard key={index} save={value}/>
                )
            })}

            <TabTrigger triggers={tabSheetRef}>
                <Button>
                    Create save
                </Button>
            </TabTrigger>

            <TabSheet ref={tabSheetRef} onDismiss={dismissSheet}>
                <h2>New save</h2>
                <FormContainer onSubmit={submitForm} error={formError} isLoading={formIsUploading}>
                    <Input id="saveName" name="saveName" label="Save name" type="text" max={100} valueChange={(value) => handleChange(value)} />
                </FormContainer>
            </TabSheet>
        </>
    )
}

export default SavePicker;