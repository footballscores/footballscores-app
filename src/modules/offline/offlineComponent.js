import React, { useEffect } from 'react';
import Header from '../generics/Header';
import {SignInButton} from '../generics/Buttons';
import { BottomCard } from '../generics/Cards';
import { resetCodeChallenge } from '../../data/token';
import { authorizeUser } from '../../data/authorization';

export const Offline = ({props}) => {

    authorizeUser(false, true);
    resetCodeChallenge();

    return(
        <>
            <Header title="myscores.io"/>

            <BottomCard title="Sign in to myscores.io" footer={<SignInButton></SignInButton>}>
                <p>You are currently offline. Sign in to use myscores.io.</p>
            </BottomCard>
        </>
    )
} 