import React, { useRef } from 'react';

export const TabTrigger = ({children, triggers}) => {

    const triggerTab = () => {
        if (triggers.current.classList.contains("invisible")) {
            triggers.current.classList.remove("invisible");
            triggers.current.classList.add("visible");
        } else {
            triggers.current.classList.remove("visible");
            triggers.current.classList.add("invisible");
        }
    }

    return(
        <div className="tab-trigger" onClick={triggerTab}>
            {children}
        </div>
    )
}

export const TabSheet = React.forwardRef((props, ref) => {

    const dismissPosition = useRef(0);

    const startDismissTouch = () => {
    }

    const startDismissMove = (event) => {
        let changed = event.changedTouches[0];
        let position = changed.screenY;
        if (dismissPosition.current === 0) {
            dismissPosition.current = position;
        } else {
            if (dismissPosition.current - position < -100) {
                props.onDismiss.call(this);
            }
        }
    }

    const endDismissTouch = () => {
        dismissPosition.current = 0;
    }

    return(
        <div className="tab tab-sheet invisible" ref={ref}>
            <div className={"dismiss-handler"}
             onTouchStart={startDismissTouch}
             onTouchMove={(event) => startDismissMove(event)}
             onTouchEnd={endDismissTouch}>
                <div className="dismiss-hint"></div>
            </div>
            <>
                {props.children}
            </>
        </div>
    )
})
