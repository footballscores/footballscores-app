import { useHistory } from "react-router"
import { config, storage } from "../../config/constants"

export const BottomCard = props => {
    return (
        <div className="card bottom-card">
            <div className="card-body">
                <h3>{props.title}</h3>
                <div className="card-content">
                    {props.children}
                </div>
            </div> 
            <div className="card-footer">
                {props.footer}
            </div>
        </div>
    )
}

export const CategoryCard = props => {

    return (
        <div className="card card-category" {...props}>
            {props.children}
        </div>
    )
}

export const SaveCard = props => {

    const history = useHistory();

    const accessSave = () => {
        localStorage.setItem(storage.SAVE_ID, props.save.id);
        history.push("/overview");
    }

    return (
        <CategoryCard onClick={accessSave}>
            <p>{props.save.name}</p>
        </CategoryCard>
    )
}