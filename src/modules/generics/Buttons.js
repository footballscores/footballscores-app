import randomBytes from "randombytes";
import { config, storage } from "../../config/constants";
import _ from "lodash";
import sha256 from 'crypto-js/sha256';
import { useState } from "react";
import PlusIcon from 'mdi-react/PlusIcon';

export const Button = props => {
    return(
        <>
            <button {...props} className="button button-primary">
                {props.children}
            </button>
        </>
    )
}

export const SignInButton = props => {

    const startAuthFlow = () => {
        let verificationCode = randomBytes(128).toString("hex");
        localStorage.setItem(storage.VERIFICATION_CODE, verificationCode);
        
        let codeChallenge = sha256(verificationCode);
        
        window.location = _.join([config.AUTH_URL, "?client_id=", config.CLIENT_ID, "&code_challenge=", codeChallenge, "&code_challenge_method=MSHEX"], "");
    } 

    return(
        <button {...props} 
            className="button button-signin"
            onClick={startAuthFlow}>
            Sign in
        </button>
    )
}

export const Fab = props => {

    const [iconClassName, setIconClassName] = useState("icon plus-open");

    const toggleFab = () => {
        if (iconClassName === "icon plus-open") {
            setIconClassName("icon plus-close");
        } else {
            setIconClassName("icon plus-open");
        }
    }

    return (
        <div className="fab" onClick={toggleFab}>
            <PlusIcon color={"#fff"} className={iconClassName}/>
        </div>
    )
}

export const DangerButton = props => {

}

export const DisabledButton = props => {

}