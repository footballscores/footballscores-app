export const LinearLoaderInfinite = ({message = null}) => {

    return(
        <> 
            {message && 
                <p>{message}</p>
            }
            <div className="loader linear">
                <div className="loader-progress"></div>
            </div>
        </>
    )
}