import { useState } from "react";
import { Button } from "./Buttons";
import React from 'react';
import { LinearLoaderInfinite } from "./Loaders";

export const Input = React.forwardRef((props, ref) => {
    const [current, setCurrent] = useState("");

    const handleChange = (event) => {
        let value = event.target.value;

        if (props.type === "text") {
            handleTextChange(value);
        } else if (props.type === "number") {
            handleNumberChange(value);
        }
    }

    const handleTextChange = (value) => {
        if (!props.max) {
            setCurrent(value);
            if (props.valueChange) {
                props.valueChange.call(this, value);
            }
            return;
        }

        if (value.length <= props.max) {
            setCurrent(value);
            if (props.valueChange) {
                props.valueChange.call(this, value);
            }
        }
    }

    const handleNumberChange = (value) => {
        if (!props.max) {
            setCurrent(value);
            if (props.valueChange) {
                props.valueChange.call(this, value);
            }
        }

        if (value <= props.max) {
            setCurrent(value);
            if (props.valueChange) {
                props.valueChange.call(this, value);
            }
        }
    }

    return (
        <div className="input-field">
            <input type={props.type}
                ref={ref}
                placeholder=" "
                id={props.id}
                name={props.name}
                onChange={(event) => handleChange(event)}
                value={current} />
            <label htmlFor={props.id}>
                {props.label}
            </label>
            {props.max && props.type === "text" &&
                <div className="max-length-hint">
                    {current.length} / {props.max}
                </div>
            }
        </div>
    )
})

export const FormContainer = (props) => {

    const submitForm = () => {
        props.onSubmit.call(this);
    }

    return (
        <div className="form-container">
            {props.isLoading &&
                <div className="submission-loader">
                    <LinearLoaderInfinite />
                </div>
            }

            {props.children}

            <Button onClick={submitForm}>
                {props.submitText || "Submit"}
            </Button>

            {props.error &&
                <p>{props.error}</p>
            }
        </div>
    )
}