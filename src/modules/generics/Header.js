import { useState } from "react";
import MenuIcon from 'mdi-react/MenuIcon';
import CloseIcon from 'mdi-react/CloseIcon';

const Header = props => {

    const [menuOpened, setMenuOpened] = useState(false);

    const toggleMenu = () => {
        setMenuOpened(!menuOpened);
        console.log(menuOpened);
    }

    return (
        <>
            <div className="header" onClick={toggleMenu} style={(props.background) ? {backgroundColor: props.background} : {}}>
                {
                    (menuOpened) ?
                        <CloseIcon color="#fff" onClick={toggleMenu} className={"menu-icon"}/>
                        :
                        <MenuIcon color="#fff" onClick={toggleMenu} className={"menu-icon"} />
                }
                <p>{props.title}</p>
            </div>
            <div className="header-spacing"></div>
        </>
    )
}

export default Header;