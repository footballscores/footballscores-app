import _ from 'lodash';
import axios from 'axios';
import { config } from '../config/constants';

export const getSaves = async (accessToken) => {
    let headers = {
        'Authorization': "Bearer " + accessToken
    };

    let response = await axios({
        method: "GET", 
        url: config.API_URL + "/user/saves",
        headers
    });

    return response;
}

export const createSave = async (accessToken, name) => {
    let headers = {
        'Authorization': "Bearer " + accessToken
    };

    console.log(name);

    let data = {
        properties: {
            name: name
        }
    }

    let response = await axios({
        method: "POST",
        url: config.API_URL + "/user/saves",
        headers,
        data
    });

    return response;
}