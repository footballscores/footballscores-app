import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import CallbackScreen from './modules/callback/CallbackScreen';
import { Offline } from './modules/offline/offlineComponent';
import { Overview } from "./modules/overview/overviewComponent"
import SavePicker from './modules/save/SavePicker';

export const AnimatedApp = (props) => {

    return(
        <Router>
            <Switch>
                <Route exact path={"/"} component={Offline}/>
                <Route path={"/overview"} component={Overview}/>
                <Route path={"/save"} component={SavePicker}/>
                <Route path={"/match/:matchId"}/>
                <Route path={"/team/:teamName"}/>
                <Route path={"/player/:playerName"}/>
                <Route path={"/manager/:managerId"}/>
                <Route path={"/callback"} component={CallbackScreen}/>
            </Switch>
        </Router>
    )
} 