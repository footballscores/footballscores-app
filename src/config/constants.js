export const storage = {
    ACCESS_TOKEN: "access_token",
    ACCESS_TOKEN_EXPIRE_DATE: "access_token_expire_date",
    VERIFICATION_CODE: "verification_code",
    CODE_CHALLENGE: "code_challenge",
    SAVE_ID: "save_id"
}

export const config = {
    BASE_URL: 'http://localhost:3000',
    API_URL: "http://localhost:8081",
    AUTH_URL: "http://localhost:8081/authorize",
    CLIENT_ID: "56b3c02ce041-24032a221eee-20ca9041b45f-131cdda73b2a"
}